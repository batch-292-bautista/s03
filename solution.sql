--add the following records to the blod_db databse;
INSERT INTO users (email, password, datetime_created) VALUES ("johnsmith@gmail.com", "passwordA","2021-01-01 01:00:00"), ("juandelacruz@gmail.com", "passwordB","2021-01-01 02:00:00"),
("janesmith@gmail.com", "passwordC","2021-01-01 03:00:00"),("mariadelacruz@gmail.com", "passwordD","2021-01-01 04:00:00"),("johndoe@gmail.com", "passwordE","2021-01-01 04:00:00");

--add the following records to the blog_db database;
INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (1, "First Code", "Hello World!", "2021-01-01 01:00:00"), (2, "Second Code", "Hello Earth!", "2021-01-01 02:00:00"), (3, "Third Code", "Welcome to Mars!", "2021-01-01 03:00:00"), (4, "Fourth Code", "Bye Bye Solar System!", "2021-01-01 04:00:00");

--get all the post with an author of 1;
SELECT * FROM posts WHERE author_id = 1;

-- get all the user's email and datetime_created
SELECT email, datetime_created FROM users;

--update a posts's content to "Hello to the peopleof the Earth!" where its inital content is "Hello Earth!" by using the record's Id
UPDATE posts SET content = "Hello to the people of the Earth!" WHERE id = 3;

--Delete the user with an email of "johndoe@gmail.com"
DELETE FROM users WHERE email = "johndoe@gmail.com";
