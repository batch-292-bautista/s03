--create a new database
CREATE DATABASE course_db;

--drop the course_db database
DROP DATABASE course_db;

--recreate a new database
CREATE DATABASE course_db;

--select the course_db database
USE course_db;

--create table
CREATE TABLE students(
  id INT NOT NULL AUTO_INCREMENT,
  username VARCHAR(50) NOT NULL,
  password VARCHAR(50) NOT NULL,
  full_name VARCHAR(50) NOT NULL,
  email VARCHAR(50),
  PRIMARY KEY (id)
);

CREATE TABLE subjects(
  id INT NOT NULL AUTO_INCREMENT,
  course_name VARCHAR(100) NOT NULL,
  schedule VARCHAR(100) NOT NULL,
  instructor VARCHAR(100) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE enrollments(
  id INT NOT NULL AUTO_INCREMENT,
  student_id INT NOT NULL,
  subject_id INT NOT NULL,
  datetime_created TIMESTAMP NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT fk_enrollments_student_id
    FOREIGN KEY (student_id)
    REFERENCES students(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT,
  CONSTRAINT fk_enrollments_subject_id
    FOREIGN KEY (subject_id)
    REFERENCES subjects(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT
);

--create record
INSERT INTO students (username, password, full_name, email) VALUES("nbautsta", "password1", "Nenita Bautista", "ne.bautsta@gmail.com");

INSERT INTO subjects (course_name, schedule, instructor) VALUES("INTRO TO MYSQL FOR MONGODB DEVELOPERS", "5:00 pm to 10:00 pm", "Ms. Camille Doroteo");

INSERT INTO enrollments (student_id, subject_id, datetime_created) VALUES (1, 1, "2023-05-02 05:00:00");