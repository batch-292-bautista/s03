--delete the reviews table
DROP TABLE reviews;

--create a new table called reviews;
CREATE  TABLE reviews(
  id INT NOT NULL AUTO_INCREMENT,
  review VARCHAR(500) NOT NULL,
  datetime_created DATETIME NOT NULL,
  ratings INT NOT NULL,
  PRIMARY KEY (id)
);

--create 5 reviews
INSERT INTO reviews (review, datetime_created, ratings) VALUES ("The songs are okay. Worth the Description", "2023-05-03 00:00:00", 5),("The songs are meh. I want BLACKPINK", "2023-01-23 00:00:00", 1),("Add Bruno Mars and Lady Gaga", "2023-03-03 00:00:00", 4), ("I want to listen more about kpop", "2023-09-23 00:00:00", 3), ("Kindly add more OPM", "2023-02-01 00:00:00", 5);

--display all review records
SELECT * FROM reviews;

--display all review records with ratings of 5
SELECT * FROM reviews WHERE ratings = 5;

--display all review records with ratings of 1
SELECT * FROM reviews WHERE ratings = 1;
  
--UPDATE ALL RATINGS TO 5
UPDATE reviews SET ratings = 5;